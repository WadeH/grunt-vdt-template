# grunt-vdt-template

> Simple replacement-based tfile templating system

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-vdt-template --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-vdt-template');
```

## The "vdt_template" task

### Overview
In your project's Gruntfile, add a section named `vdt_template` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  vdt_template: {
    options: {
      // Task-specific options go here. (there aren't currently any)
    },
    files: [
      // Templates
      { src: 'source_template_file', dest: 'destination_file_name', json: 'json_with_field_array', jsonSrc: 'json_array_name'}
    ]
  },
});
```

### Options

There aren't currently any options

### Usage Examples

#### Default Options
In this example, the default options are used to do something with whatever. So if the `testing` file has the content `Testing` and the `123` file had the content `1 2 3`, the generated result would be `Testing, 1 2 3.`

```js
grunt.initConfig({
  vdt_template: {
    options: {},
    files: [
      { src: 'page_template.html', dest: 'dist/about.html', json: 'template_values.json', jsonSrc: 'about'}
    ]
  },
});
```

page_template.html
```html
<div style='width:320px; border: 1px solid black; text-align:center'>%title%</div>
<div style'width: 320px; border: 1px solid black;'>
  <div style='width:100%; text-align:center'>
    <b>%appname$</b> v%version.major%.%version.minor%
  </div>
</div>
```

template_values.json
```json
{
  "about": [
    ["%appname%", "Sample Application"],
    ["%version.major%", "1"],
    ["%version.minor%","0"]
  ]
}
```
## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_
