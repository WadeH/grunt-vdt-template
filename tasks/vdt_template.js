/*
 * grunt-vdt-template
 * 
 *
 * Copyright (c) 2015 Wade Harkins
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('vdt_template', 'Simple replacement-based tfile templating system', function() {
    // Merge task-specific and/or target-specific options with these defaults.
   /* var options = this.options({
      placeholder: '%'
    });*/

    this.files.forEach(function(t){
      var templData = grunt.file.read(t.src);
      var values = grunt.file.readJSON(t.json)[t.jsonSrc];
      values.forEach(function(ph){
        var tmplVar = ph[0];
        var tmplVal = ph[1];
        templData = templData.replace(tmplVar, tmplVal);
      });
      grunt.file.write(t.dest, templData);
      //grunt.log("Applied values from " + t.json + " to " + t.src " => " t.dest);
    });
  });

};
